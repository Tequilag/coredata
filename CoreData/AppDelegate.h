//
//  AppDelegate.h
//  CoreData
//
//  Created by Vladislav Grigoriev on 30/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end

